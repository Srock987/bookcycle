package com.srock.bookcycle.domain.bookProposition;

import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.data.entities.RequestedBookProposition;
import com.srock.bookcycle.data.repositories.GeneratedPropositionRepository;
import com.srock.bookcycle.data.repositories.RequestedPropositionRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-09-29.
 */

public class RequestAndListenUseCase {

    private GeneratedPropositionRepository generatedPropositionRepository;
    private RequestedPropositionRepository requestedPropositionRepository;

    public RequestAndListenUseCase(GeneratedPropositionRepository generatedPropositionRepository, RequestedPropositionRepository requestedPropositionRepository){
        this.generatedPropositionRepository = generatedPropositionRepository;
        this.requestedPropositionRepository = requestedPropositionRepository;
    }

    public Flowable<Key<GeneratedBookProposition>> listenForGeneratedFromPhoto(Key<Photo> photoKey){
        return requestedPropositionRepository.insertRequetedProposition(RequestedBookProposition.create(photoKey.value().uri()),photoKey.key())
                .toFlowable()
                .flatMap(requestedBookPropositionKey -> generatedPropositionRepository.listenForUpdate(requestedBookPropositionKey.key()));
    }

    public Single<Key<GeneratedBookProposition>> updateGeneratedProposition(Key<Photo> photoKey){
        return generatedPropositionRepository.updateGeneratedProposition(photoKey);
    }


}
