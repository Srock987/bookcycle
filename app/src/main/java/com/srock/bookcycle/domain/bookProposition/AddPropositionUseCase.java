package com.srock.bookcycle.domain.bookProposition;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.Single;

/**
 * Created by Pawel on 2017-10-02.
 */

public class AddPropositionUseCase {

    private BookRepository bookRepository;

    public AddPropositionUseCase(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    public Single<Key<BookProposition>> addBookProposition(BookProposition bookProposition){
       return bookRepository.insertBookProposition(bookProposition);
    }
}
