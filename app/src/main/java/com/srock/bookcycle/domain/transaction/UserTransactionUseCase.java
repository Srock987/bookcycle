package com.srock.bookcycle.domain.transaction;

import com.google.firebase.auth.FirebaseAuth;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.filters.TransactionHelper;
import com.srock.bookcycle.data.repositories.TransactionRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by pawelsrokowski on 25/10/2017.
 */

public class UserTransactionUseCase {

    private TransactionRepository transactionRepository;

    public UserTransactionUseCase(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Flowable<List<Key<Transaction>>> getCurrentUserCreatedTransactions(){
        return transactionRepository.getCreatedUserTransaction(FirebaseAuth.getInstance().getCurrentUser().getUid()).toFlowable();
    }

    public Flowable<List<Key<Transaction>>> getCurrentUserIncomingTransactions(){
        return transactionRepository.getIncomingUserTransaction(FirebaseAuth.getInstance().getCurrentUser().getUid()).toFlowable();
    }
}
