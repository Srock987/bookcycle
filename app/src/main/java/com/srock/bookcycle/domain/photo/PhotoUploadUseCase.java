package com.srock.bookcycle.domain.photo;

import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.data.repositories.GeneratedPropositionRepository;
import com.srock.bookcycle.data.repositories.PhotoRepository;
import com.srock.bookcycle.data.repositories.PhotoStorageRepository;

import java.io.File;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-09-15.
 */

public class PhotoUploadUseCase {

    private static final String BOOK_PROPOSITION_STORAGE_DIRECTORY = "bookPropositions";

    private final PhotoRepository photoRepository;
    private final PhotoStorageRepository photoStorageRepository;

    public PhotoUploadUseCase(PhotoRepository photoRepository, PhotoStorageRepository photoStorageRepository){
        this.photoRepository = photoRepository;
        this.photoStorageRepository = photoStorageRepository;
    }

    public Single<Key<Photo>> insertPropositionPhoto(File file) {
        return photoRepository.insertPhoto()
                .flatMap(photoKey ->
                        photoStorageRepository.insertPhoto(BOOK_PROPOSITION_STORAGE_DIRECTORY,photoKey.key(),file)
                                .flatMap(taskSnapshot -> photoRepository.updatePhoto(Key.of(photoKey.key(),Photo.create(taskSnapshot.getDownloadUrl().toString(), Photo.UploadStatus.FINISHED)))));
    }

    public Completable removePropositionPhoto(String photoId){
        return photoRepository.removePhoto(photoId).andThen(photoStorageRepository.removePhoto(BOOK_PROPOSITION_STORAGE_DIRECTORY,photoId));
    }


}
