package com.srock.bookcycle.domain.bookProposition;

import com.google.firebase.auth.FirebaseAuth;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by pawelsrokowski on 25/10/2017.
 */

public class GetUserPropositionsUseCase {

    private BookRepository bookRepository;

    public GetUserPropositionsUseCase(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Flowable<List<Key<BookProposition>>> getCurrentUserBooks(){
       return bookRepository.userBooks(FirebaseAuth.getInstance().getCurrentUser().getUid()).toFlowable();
    }

    public Flowable<List<Key<BookProposition>>> getUserBooks(String uId){
        return bookRepository.userBooks(uId).toFlowable();
    }

}
