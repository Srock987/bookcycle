package com.srock.bookcycle.domain.adress;

import com.google.firebase.auth.FirebaseAuth;
import com.srock.bookcycle.data.entities.Address;
import com.srock.bookcycle.data.repositories.AddressRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.Single;

/**
 * Created by Pawel on 2017-10-01.
 */

public class AddressUseCase {

    private AddressRepository addressRepository;

    public AddressUseCase(AddressRepository addressRepository){
        this.addressRepository = addressRepository;
    }

    public Single<Key<Address>> getUserAdress(String uId){
        return addressRepository.address(uId);
    }
}
