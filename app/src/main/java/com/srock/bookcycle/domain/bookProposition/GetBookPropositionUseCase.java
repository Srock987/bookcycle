package com.srock.bookcycle.domain.bookProposition;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.Single;

public class GetBookPropositionUseCase {

    private BookRepository bookRepository;

    public GetBookPropositionUseCase(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Single<Key<BookProposition>> getBookProposition(String bookId){
        return bookRepository.bookProposition(bookId).toSingle();
    }
}
