package com.srock.bookcycle.domain.transaction;

import com.google.firebase.auth.FirebaseAuth;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.filters.TransactionHelper;
import com.srock.bookcycle.data.repositories.TransactionRepository;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.Single;

/**
 * Created by pawelsrokowski on 24/10/2017.
 */

public class TransactionInsertUseCase {

    private TransactionRepository transactionRepository;

    public TransactionInsertUseCase(TransactionRepository transactionRepository) {
        this.transactionRepository = transactionRepository;
    }

    public Single<Key<Transaction>> insertTransactionFromBookProposition(Key<BookProposition> bookPropositionKey){
        return insertTransaction(TransactionHelper.createTransaction(bookPropositionKey, FirebaseAuth.getInstance().getCurrentUser().getUid()));
    }

    private Single<Key<Transaction>> insertTransaction(Transaction transaction){
        return transactionRepository.insertNewTransaction(transaction);
    }
}

