package com.srock.bookcycle.utils.Filters;

/**
 * Created by Pawel on 2017-07-11.
 */

public class AddressSurveyFilterUtils {

    public static String getFormatedPostalCode(String inputPostalCode){
        String regex = "[^[0-9]-]";
        inputPostalCode = inputPostalCode.replaceAll(regex,"");
        return inputPostalCode;
    }

    public static String getFormatedCity(String inputCity){
        String regex = "[[0-9]-_\\s]";
        inputCity = inputCity.replaceAll(regex,"");
        return inputCity;
    }

}
