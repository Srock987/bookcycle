package com.srock.bookcycle.utils.Rx;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.subjects.AsyncSubject;


/**
 * Created by srokowski.maciej@gmail.com on 02.12.16.
 */

public class RxTask {

    private RxTask() {
    }

    public static <R extends Task<T>, T> Observable<T> observable(R task) {
        AsyncSubject<T> asyncSubject = AsyncSubject.create();
        task.addOnCompleteListener(new OnCompleteListener<T>() {
            @Override
            public void onComplete(@NonNull Task<T> task) {
                if (task.isSuccessful()) {
                    asyncSubject.onNext(task.getResult());
                    asyncSubject.onComplete();
                } else {
                    asyncSubject.onError(task.getException());
                }
            }
        });
        return asyncSubject;
    }

    public static <R extends Task<Void>, T> Observable<T> observable(R task, T valueToReturn) {
        AsyncSubject<T> asyncSubject = AsyncSubject.create();
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    asyncSubject.onNext(valueToReturn);
                    asyncSubject.onComplete();
                } else {
                    asyncSubject.onError(task.getException());
                }
            }
        });
        return asyncSubject;
    }

    public static <R extends Task<Void>> Completable completable(R task) {
        AsyncSubject<Void> asyncSubject = AsyncSubject.create();
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    asyncSubject.onComplete();
                } else {
                    asyncSubject.onError(task.getException());
                }
            }
        });
        return Completable.fromObservable(asyncSubject);
    }


}
