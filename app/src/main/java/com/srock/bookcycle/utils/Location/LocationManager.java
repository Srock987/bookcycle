package com.srock.bookcycle.utils.Location;

import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;

/**
 * Created by Pawel on 2017-05-29.
 */

public class LocationManager {

    private GoogleApiClient mGoogleApiClient;

    public LocationManager(Context context){
        mGoogleApiClient = new GoogleApiClient.Builder(context).addApi(Places.GEO_DATA_API).build();
    }

}
