package com.srock.bookcycle.fragments;

import android.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.R;
import com.srock.bookcycle.fragments.fragmentsPresenters.UploadPresenter;
import com.srock.bookcycle.fragments.fragmentsViews.UploadPhotoView;
import com.srock.bookcycle.utils.Firebase.Key;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pawelsrokowski on 14/09/2017.
 */

public class UploadPhotoFragment extends Fragment implements UploadPhotoView {

    @BindView(R.id.uploadPhotoImageView)
    ImageView photoImageView;
    @BindView(R.id.updateButton)
    Button updateButton;
    @BindView(R.id.uploadStatusTextView)
    TextView uploadStatusTextView;

    private UploadPresenter uploadPresenter = new UploadPresenter(this);

    public void setArguments(Bundle bundle){
        uploadPresenter.loadFile(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_upload_photo,container,false);
        ButterKnife.bind(this,rootView);
        photoImageView.setImageBitmap(uploadPresenter.getPhotoBitmap());
        uploadPresenter.startUploadingPhoto(getActivity());
        return rootView;
    }


    @Override
    public void finishedUploading(Key<Photo> photoKey) {
        updateButton.setVisibility(View.VISIBLE);
        updateButton.setOnClickListener(view -> uploadPresenter.updateGeneratedProposition(photoKey));
        uploadStatusTextView.setText(R.string.generateing_book_proposition);
    }

    @Override
    public void receivedGeneratedProposition(Key<GeneratedBookProposition> generatedBookPropositionKey) {
        SubmitPropositionFragment fragment = new SubmitPropositionFragment();
        fragment.setData(generatedBookPropositionKey.value());
        getActivity().getFragmentManager().beginTransaction().replace(R.id.propositionFragment,fragment).commit();
    }

}
