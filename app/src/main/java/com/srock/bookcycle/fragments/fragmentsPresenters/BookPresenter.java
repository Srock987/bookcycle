package com.srock.bookcycle.fragments.fragmentsPresenters;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.fragments.fragmentsViews.BookView;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by Pawel on 2017-10-02.
 */

public class BookPresenter {

    private BookView bookView;
    private Key<BookProposition> bookPropositionKey;

    public void setBookPropositionKey(Key<BookProposition> bookPropositionKey){
        this.bookPropositionKey = bookPropositionKey;
    }

    public Key<BookProposition> getBookPropositionKey() {
        return bookPropositionKey;
    }

    public BookPresenter(BookView bookView){
        this.bookView = bookView;
    }

    public void showBookProposition(){
        bookView.setTitle(bookPropositionKey.value().bookName());
        bookView.setAuthor(bookPropositionKey.value().author());
        bookView.setDescription(bookPropositionKey.value().bookDescription());
        bookView.setImageView(bookPropositionKey.value().picLink());
    }

}
