package com.srock.bookcycle.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.R;
import com.srock.bookcycle.fragments.fragmentsPresenters.BookPresenter;
import com.srock.bookcycle.fragments.fragmentsViews.BookView;
import com.srock.bookcycle.utils.Firebase.Key;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pawel on 2017-07-14.
 */

public class BookPropositionFragment extends Fragment implements BookView {

    private BookPresenter bookPresenter = new BookPresenter(this);

    @BindView(R.id.bookTitleTextView)
    TextView bookTitleTextView;
    @BindView(R.id.bookAuthorTextView)
    TextView bookAuthorTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.bookCoverImageView)
    ImageView bookCoverImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_book_proposition,container,false);
        ButterKnife.bind(this, rootView);
        bookPresenter.showBookProposition();
        return rootView;
    }


    public void setBookProposition(Key<BookProposition> bookPropositionKey){
        bookPresenter.setBookPropositionKey(bookPropositionKey);
    }

    public Key<BookProposition> getBookProposition(){
        return bookPresenter.getBookPropositionKey();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void setTitle(String title) {
        bookTitleTextView.setText(title);
    }

    @Override
    public void setAuthor(String author) {
        bookAuthorTextView.setText(author);
    }

    @Override
    public void setDescription(String description) {
        descriptionTextView.setText(description);
    }

    @Override
    public void setImageView(String pictureURL) {
       Glide.with(this).load(pictureURL).into(bookCoverImageView);
    }

}
