package com.srock.bookcycle.fragments.fragmentsViews;

import com.srock.bookcycle.fragments.BookPropositionFragment;

/**
 * Created by Pawel on 2017-10-01.
 */

public interface SubmitView {
    void showBookProposition(BookPropositionFragment bookPropositionFragment);
    void onSubmissionSuccess();
    void setSubmissionButtons();
}
