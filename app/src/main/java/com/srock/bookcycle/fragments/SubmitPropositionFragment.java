package com.srock.bookcycle.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.srock.bookcycle.R;
import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.fragments.fragmentsPresenters.SubmitPresenter;
import com.srock.bookcycle.fragments.fragmentsViews.SubmitView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pawel on 2017-10-01.
 */

public class SubmitPropositionFragment extends Fragment implements SubmitView {

    @BindView(R.id.acceptSubmission)
    Button acceptSubmissionButton;
    @BindView(R.id.changeSubmission)
    Button changeSubmissionButton;

    private SubmitPresenter submitPresenter = new SubmitPresenter(this);

    public void setData(GeneratedBookProposition generatedBookProposition){
        submitPresenter.startFragmentCreation(generatedBookProposition);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_submit_proposition,container,false);
        ButterKnife.bind(this,rootView);
        return rootView;
    }


    @Override
    public void showBookProposition(BookPropositionFragment bookPropositionFragment) {
        getFragmentManager().beginTransaction().
                replace(R.id.bookPropositionPaceholder,bookPropositionFragment).commit();
    }

    @Override
    public void onSubmissionSuccess() {
        Toast.makeText(getActivity(),"Book proposition added",Toast.LENGTH_LONG).show();
        getActivity().finish();
    }

    @Override
    public void setSubmissionButtons() {
        acceptSubmissionButton.setOnClickListener(view -> {
            submitPresenter.submitBookProposition();
        });
    }
}
