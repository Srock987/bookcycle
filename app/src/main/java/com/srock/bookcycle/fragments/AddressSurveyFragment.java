package com.srock.bookcycle.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.srock.bookcycle.data.entities.Address;
import com.srock.bookcycle.R;;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pawel on 2017-05-29.
 */

public class AddressSurveyFragment extends Fragment {

    private Address address;
    @BindView(R.id.name_editText)
    EditText nameEditText;
    @BindView(R.id.country_editText)
    EditText countryEditText;
    @BindView(R.id.region_editText)
    EditText regionEditText;
    @BindView(R.id.postal_code_editText)
    EditText postalEditText;
    @BindView(R.id.city_editText)
    EditText cityEditText;
    @BindView(R.id.street_adress_editText)
    EditText streetEditText;
    @BindView(R.id.confirm_buttom)
    Button confirmButton;

    private AddressFragmentConfirmationListener addressFragmentConfirmationListener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_adress_form, container, false);
        ButterKnife.bind(this, rootView);
        fillInForms(address);
        setUpConfirmationButton();
        return rootView;
    }

    private void setUpConfirmationButton(){
        confirmButton.setOnClickListener(v -> {
            addressFragmentConfirmationListener.onConfirmationClicked(getAddress());
        });
    }

    private void fillInForms(Address viewModel) {
        nameEditText.setText(viewModel.name());
        countryEditText.setText(viewModel.country());
        regionEditText.setText(viewModel.region());
        postalEditText.setText(viewModel.postalCode());
        cityEditText.setText(viewModel.city());
        streetEditText.setText(viewModel.streetAddress());
    }

    private Address getAddress() {
        updateAddress();
        return address;
    }

    private void updateAddress() {
        this.address = Address.create(nameEditText.getText().toString(),
                countryEditText.getText().toString(),
                regionEditText.getText().toString(),
                postalEditText.getText().toString(),
                cityEditText.getText().toString(),
                streetEditText.getText().toString(),
                FirebaseAuth.getInstance().getCurrentUser().getUid()
        );
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setAddressFragmentConfirmationListener(AddressFragmentConfirmationListener addressFragmentConfirmationListener) {
        this.addressFragmentConfirmationListener = addressFragmentConfirmationListener;
    }
}
