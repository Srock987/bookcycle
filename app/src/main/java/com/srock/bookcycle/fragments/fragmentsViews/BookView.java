package com.srock.bookcycle.fragments.fragmentsViews;

/**
 * Created by Pawel on 2017-10-02.
 */

public interface BookView {
    void setTitle(String title);
    void setAuthor(String author);
    void setDescription(String description);
    void setImageView(String pictureURL);
}
