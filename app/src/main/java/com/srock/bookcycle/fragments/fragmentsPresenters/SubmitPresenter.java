package com.srock.bookcycle.fragments.fragmentsPresenters;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.repositories.AddressRepository;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.data.utils.GeneratedPropositionHelper;
import com.srock.bookcycle.domain.adress.AddressUseCase;
import com.srock.bookcycle.domain.bookProposition.AddPropositionUseCase;
import com.srock.bookcycle.fragments.BookPropositionFragment;
import com.srock.bookcycle.fragments.fragmentsViews.SubmitView;
import com.srock.bookcycle.utils.Firebase.Key;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Pawel on 2017-10-01.
 */

public class SubmitPresenter {

    private final static String TAG = "SubmitPresenter";
    private CompositeDisposable subscriptions = new CompositeDisposable();
    private SubmitView submitView;
    private BookPropositionFragment fragment;

    public SubmitPresenter(SubmitView submitView){
        this.submitView = submitView;
    }

    public void startFragmentCreation(GeneratedBookProposition generatedBookProposition){
        String currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        AddressUseCase addressUseCase = new AddressUseCase(new AddressRepository(FirebaseDatabase.getInstance()));
        subscriptions.add(addressUseCase.getUserAdress(currentUserId)
                .map(addressKey ->
                        GeneratedPropositionHelper.fromGeneratedToBook(
                                generatedBookProposition,
                                currentUserId,
                                addressKey.value().city()))
                .subscribe(bookProposition -> {
                    fragment = new BookPropositionFragment();
                    fragment.setBookProposition(Key.of("EMPTY_KEY",bookProposition));
                    submitView.showBookProposition(fragment);
                    submitView.setSubmissionButtons();
                }));
    }

    public void submitBookProposition(){
        AddPropositionUseCase useCase = new AddPropositionUseCase(new BookRepository(FirebaseDatabase.getInstance()));
        subscriptions.add(useCase.addBookProposition(fragment.getBookProposition().value()).subscribe(bookPropositionKey -> {
            submitView.onSubmissionSuccess();
        }));
    }

}
