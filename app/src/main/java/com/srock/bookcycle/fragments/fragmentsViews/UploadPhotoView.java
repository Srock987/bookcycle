package com.srock.bookcycle.fragments.fragmentsViews;

import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by Pawel on 2017-10-01.
 */

public interface UploadPhotoView {
    void finishedUploading(Key<Photo> photoKey);
    void receivedGeneratedProposition(Key<GeneratedBookProposition> generatedBookPropositionKey);
}
