package com.srock.bookcycle.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.srock.bookcycle.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by pawelsrokowski on 14/09/2017.
 */

public class TakePhotoFragment extends Fragment {

    public static final String TAG = "BOOK_PHOTO_FRAGMENT";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_STORAGE_PERMISSIONS = 2;
    static final int REQUEST_CAMERA_PERMISSIONS = 3;

    @BindView(R.id.takePhotoButton)
    ImageButton takePhotoButton;
    private Uri photoURI;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_book_photo,container,false);
        ButterKnife.bind(this,rootView);
        setViews();
        return rootView;
    }

    private void setViews() {
        takePhotoButton.setOnClickListener(view -> {
            checkStoragePermission();
        });
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_STORAGE_PERMISSIONS);
        } else {
            checkCameraPermission();
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkCameraPermission(){
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
            requestPermissions(new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSIONS);
        } else {
            requestImageCapture();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
             startUploadPhotoFragment(Uri.fromFile(imageFile));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_STORAGE_PERMISSIONS){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                checkCameraPermission();
            }
        } else if (requestCode == REQUEST_CAMERA_PERMISSIONS){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                requestImageCapture();
            }
        }
        Log.d(TAG,"Permission result");
    }

    private void startUploadPhotoFragment(Uri uri){

        Bundle bundle = new Bundle();
        bundle.putParcelable("uri",uri);

        // Creating uploadFragment and injecting byteArray
        UploadPhotoFragment uploadPhotoFragment = new UploadPhotoFragment();
        uploadPhotoFragment.setArguments(bundle);

        getActivity().getFragmentManager().beginTransaction().replace(R.id.propositionFragment,uploadPhotoFragment).commit();
    }

    private void requestImageCapture(){
        EasyImage.openCamera(this,REQUEST_IMAGE_CAPTURE);
    }
}
