package com.srock.bookcycle.fragments;

import com.srock.bookcycle.data.entities.Address;

/**
 * Created by Pawel on 2017-07-14.
 */

public interface AddressFragmentConfirmationListener {
    public void onConfirmationClicked(Address address);
}
