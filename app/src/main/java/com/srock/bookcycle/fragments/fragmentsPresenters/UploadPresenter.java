package com.srock.bookcycle.fragments.fragmentsPresenters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.data.repositories.GeneratedPropositionRepository;
import com.srock.bookcycle.data.repositories.PhotoRepository;
import com.srock.bookcycle.data.repositories.PhotoStorageRepository;
import com.srock.bookcycle.data.repositories.RequestedPropositionRepository;
import com.srock.bookcycle.domain.bookProposition.RequestAndListenUseCase;
import com.srock.bookcycle.domain.photo.PhotoUploadUseCase;
import com.srock.bookcycle.fragments.fragmentsViews.UploadPhotoView;
import com.srock.bookcycle.utils.Firebase.Key;

import java.io.File;

import id.zelory.compressor.Compressor;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Pawel on 2017-10-01.
 */

public class UploadPresenter {

    private final static String UPLOAD_DATA_HANDLER = "UPLOAD_DATA_HANDLER";

    private UploadPhotoView uploadPhotoView;

    private File imageFile;
    private PhotoUploadUseCase uploadUseCase;
    private RequestAndListenUseCase requestAndListenUseCase;
    private CompositeDisposable subscriptions = new CompositeDisposable();

    public UploadPresenter(UploadPhotoView uploadPhotoView) {
        this.uploadPhotoView = uploadPhotoView;
    }

    public void loadFile(Bundle bundle) {
        Uri uri = bundle.getParcelable("uri");
        if (uri != null) {
            imageFile = new File(uri.getPath());
        }
    }

    public Bitmap getPhotoBitmap() {
        return BitmapFactory.decodeFile(imageFile.getPath());
    }

    public void updateGeneratedProposition(Key<Photo> photoKey) {
        requestAndListenUseCase.updateGeneratedProposition(photoKey).subscribe(generatedBookPropositionKey -> {
            Log.d(UPLOAD_DATA_HANDLER, "Photo updated");
        });
    }

    public void startUploadingPhoto(Context context) {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        uploadUseCase = new PhotoUploadUseCase(new PhotoRepository(firebaseDatabase),
                new PhotoStorageRepository(FirebaseStorage.getInstance(), "PROPOSITION_PHOTOS", new Compressor(context)));
        requestAndListenUseCase = new RequestAndListenUseCase(new GeneratedPropositionRepository(firebaseDatabase),
                new RequestedPropositionRepository(firebaseDatabase));

        subscriptions.add(uploadUseCase.insertPropositionPhoto(imageFile)
                .doOnSuccess(photoKey -> uploadPhotoView.finishedUploading(photoKey))
                .flatMapPublisher(photoKey -> requestAndListenUseCase.listenForGeneratedFromPhoto(photoKey))
                .subscribe(generatedProposition -> {
                    Log.d(UPLOAD_DATA_HANDLER, "Uploading finished");
                    uploadPhotoView.receivedGeneratedProposition(generatedProposition);
                }));
    }
}
