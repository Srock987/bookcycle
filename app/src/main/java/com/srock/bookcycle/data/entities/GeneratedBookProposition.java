package com.srock.bookcycle.data.entities;

import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.utils.Firebase.Key;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-09-28.
 */
@AutoValue
@FirebaseValue
public abstract class GeneratedBookProposition {

    public static final String BOOK_NAME_FIELD = "bookName";
    public static final String AUTHOR_FIELD = "author";
    public static final String BOOK_TYPE_FIELD = "bookType";
    public static final String BOOK_DESCRIPTION_FIELD = "bookDescription";
    public static final String PICTURE_URL_FIELD = "picLink";

    @PropertyName(BOOK_NAME_FIELD)
    public  abstract String bookName();
    @PropertyName(AUTHOR_FIELD)
    public  abstract String author();
    @PropertyName(BOOK_TYPE_FIELD)
    public  abstract String bookType();
    @PropertyName(BOOK_DESCRIPTION_FIELD)
    public  abstract String bookDescription();
    @PropertyName(PICTURE_URL_FIELD)
    public  abstract String picLink();

    public static GeneratedBookProposition create(String bookName, String author, String bookType, String bookDescription, String picLink) {
        return new AutoValue_GeneratedBookProposition(bookName, author, bookType, bookDescription, picLink);
    }

    public static Key<GeneratedBookProposition> create(DataSnapshot dataSnapshot){
        return Key.of(dataSnapshot.getKey(),dataSnapshot.getValue(AutoValue_GeneratedBookProposition.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_GeneratedBookProposition.FirebaseValue(this);
    }
}
