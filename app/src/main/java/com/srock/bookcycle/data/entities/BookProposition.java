package com.srock.bookcycle.data.entities;

import android.util.Log;

import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.data.utils.BookMocker;
import com.srock.bookcycle.utils.Firebase.Key;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-05-11.
 */

@AutoValue
@FirebaseValue
public abstract class BookProposition {


    public static final String BOOK_NAME_FIELD = "bookName";
    public static final String AUTHOR_FIELD = "author";
    public static final String BOOK_TYPE_FIELD = "bookType";
    public static final String BOOK_DESCRIPTION_FIELD = "bookDescription";
    public static final String PICTURE_URL_FIELD = "picLink";
    public static final String USER_PROPOSING_FIELD = "userProposingId";
    public static final String ORIGIN_CITY_FIELD = "originCity";

    @PropertyName(BOOK_NAME_FIELD)
    public  abstract String bookName();
    @PropertyName(AUTHOR_FIELD)
    public  abstract String author();
    @PropertyName(BOOK_TYPE_FIELD)
    public  abstract String bookType();
    @PropertyName(BOOK_DESCRIPTION_FIELD)
    public  abstract String bookDescription();
    @PropertyName(PICTURE_URL_FIELD)
    public  abstract String picLink();
    @PropertyName(USER_PROPOSING_FIELD)
    public  abstract String userProposingId();
    @PropertyName(ORIGIN_CITY_FIELD)
    public  abstract String originCity();

    public static BookProposition create(String bookName,String author
            , String bookType, String bookDescription, String pickLink,String proposingUserId, String originCity){
        return new AutoValue_BookProposition(bookName,author,bookType,bookDescription,pickLink,proposingUserId,originCity);
    }

    public static Key<BookProposition> create(DataSnapshot dataSnapshot){
        AutoValue_BookProposition.FirebaseValue value = dataSnapshot.getValue(AutoValue_BookProposition.FirebaseValue.class);
        if (value!=null){
            return Key.of(dataSnapshot.getKey(),value.toAutoValue());
        }else {
            return Key.of(dataSnapshot.getKey(), BookMocker.getBookMock());
        }
    }

    public Object toFirebaseValue() {
        return new AutoValue_BookProposition.FirebaseValue(this);
    }
}
