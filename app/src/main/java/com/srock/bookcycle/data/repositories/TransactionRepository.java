package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.utils.Rx.RxDatabaseUtils;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by pawelsrokowski on 18/10/2017.
 */

public class TransactionRepository {

    private final static String TRANSACTION_REPOSITORY_NODE = "transactions";

    private FirebaseDatabase firebaseDatabase;

    public TransactionRepository(FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference transactionsNode(){
        return firebaseDatabase.getReference(TRANSACTION_REPOSITORY_NODE);
    }

    public Single<Key<Transaction>> insertNewTransaction(Transaction transaction){
        DatabaseReference push = transactionsNode().push();
        return RxFirebaseDatabase.setValue(push,transaction.toFirebaseValue()).toSingleDefault(Key.of(push.getKey(),transaction));
    }

    public Single<Key<Transaction>> updateTransaction(Key<Transaction> transactionKey){
        return RxFirebaseDatabase.setValue(transactionsNode().child(transactionKey.key()),transactionKey.value().toFirebaseValue()).toSingleDefault(transactionKey);
    }

    public Maybe<List<Key<Transaction>>> getCreatedUserTransaction(String userId) {
        return RxFirebaseDatabase.observeSingleValueEvent(transactionsNode().orderByChild(Transaction.CREATOR_ID).equalTo(userId),
                RxDatabaseUtils.childListMapper(Transaction::create));
    }

    public Maybe<List<Key<Transaction>>> getIncomingUserTransaction(String userId) {
        return RxFirebaseDatabase.observeSingleValueEvent(transactionsNode().orderByChild(Transaction.RECIPIENT_ID).equalTo(userId),
                RxDatabaseUtils.childListMapper(Transaction::create));
    }
}
