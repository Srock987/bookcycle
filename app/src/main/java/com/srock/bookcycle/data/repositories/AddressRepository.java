package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.Address;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-07-11.
 */

public class AddressRepository {

    public static final String ADDRESSES_NODE = "addresses";

    private FirebaseDatabase firebaseDatabase;

    public AddressRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference addressNode(){
        return firebaseDatabase.getReference(ADDRESSES_NODE);
    }

    public Single<Key<Address>> address(String uId){
        return RxFirebaseDatabase.observeSingleValueEvent(addressNode().child(uId)
                ,Address::create).toSingle();
    }

    public Single<Key<Address>> insertAddress(String uId, Address address){
        DatabaseReference push = addressNode().push();
        return RxFirebaseDatabase.setValue(addressNode().child(uId),address.toFirebaseValue())
                .toSingleDefault(Key.of(uId,address));
    }

    public Single<Key<Address>> updateAddress(Key<Address> addressKey){
        return RxFirebaseDatabase.setValue(addressNode().child(addressKey.key()),addressKey.value())
                .toSingleDefault(addressKey);
    }

}
