package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.Photo;
import com.srock.bookcycle.utils.Firebase.Key;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-09-15.
 */

public class PhotoRepository {

    private static final  String PHOTOS_NODE = "photos";
    private final FirebaseDatabase firebaseDatabase;

    public PhotoRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference photosNode(){
        return firebaseDatabase.getReference(PHOTOS_NODE);
    }

    public Maybe<Key<Photo>> getPhoto(String photoId){
        return RxFirebaseDatabase.observeSingleValueEvent(photosNode().equalTo(photoId),Photo::create);
    }

    public Single<Key<Photo>> insertPhoto(){
        DatabaseReference push = photosNode().push();
        Photo insertedPhoto = Photo.create(null, Photo.UploadStatus.NOT_STARTED);
        return RxFirebaseDatabase.setValue(push,insertedPhoto.toFirebaseValue())
                .toSingleDefault(Key.of(push.getKey(),insertedPhoto));
    }

    public Single<Key<Photo>> updatePhoto(Key<Photo> photoKey){
        return RxFirebaseDatabase.setValue(photosNode().child(photoKey.key()),photoKey.value().toFirebaseValue()).toSingleDefault(photoKey);
    }

        public Completable removePhoto(String photoId){
            return RxFirebaseDatabase.setValue(photosNode().child(photoId),null);
    }
}
