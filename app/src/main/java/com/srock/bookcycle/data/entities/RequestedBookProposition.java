package com.srock.bookcycle.data.entities;

import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.utils.Firebase.Key;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-09-28.
 */
@AutoValue
@FirebaseValue
public abstract class RequestedBookProposition {
    public static final String PICTURE_URL_FIELD = "picLink";
    @PropertyName(PICTURE_URL_FIELD)
    public  abstract String picLink();

    public static RequestedBookProposition create(String picLink) {
        return new AutoValue_RequestedBookProposition(picLink);
    }

    public static Key<RequestedBookProposition> create(DataSnapshot dataSnapshot){
        return Key.of(dataSnapshot.getKey(),dataSnapshot.getValue(AutoValue_RequestedBookProposition.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_RequestedBookProposition.FirebaseValue(this);
    }
}
