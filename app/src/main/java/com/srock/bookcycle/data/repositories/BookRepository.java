package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.utils.Rx.RxDatabaseUtils;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Maybe;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-05-28.
 */

public class BookRepository {

    public static final String BOOKS_NODE = "bookPropositions";

    private FirebaseDatabase firebaseDatabase;

    public BookRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference bookPropositionsNode(){
        return firebaseDatabase.getReference(BOOKS_NODE);
    }

    public Maybe<Key<BookProposition>> bookProposition(String bookId){
        return RxFirebaseDatabase.observeSingleValueEvent(bookPropositionsNode().child(bookId),BookProposition::create);
    }

    public Maybe<List<Key<BookProposition>>> userBooks(String userId){
        return RxFirebaseDatabase.observeSingleValueEvent(bookPropositionsNode().orderByChild(BookProposition.USER_PROPOSING_FIELD).equalTo(userId),
                RxDatabaseUtils.childListMapper(BookProposition::create));
    }

    public Maybe<List<Key<BookProposition>>> cityBooks(String city){
        return RxFirebaseDatabase.observeSingleValueEvent(bookPropositionsNode().orderByChild(BookProposition.ORIGIN_CITY_FIELD).equalTo(city),
                RxDatabaseUtils.childListMapper(BookProposition::create));
    }


    public Single<Key<BookProposition>> insertBookProposition(BookProposition bookProposition){
        DatabaseReference push = bookPropositionsNode().push();
        return RxFirebaseDatabase.setValue(push,bookProposition.toFirebaseValue())
                .toSingleDefault(Key.of(push.getKey(),bookProposition));
    }

    public Single<Key<BookProposition>> updateBookProposition(Key<BookProposition> bookPropositionKey){
        return RxFirebaseDatabase.setValue(bookPropositionsNode().child(bookPropositionKey.key()),
                bookPropositionKey.value().toFirebaseValue())
                .toSingleDefault(bookPropositionKey);
    }


}
