package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.data.entities.User;
import com.srock.bookcycle.utils.Rx.RxDatabaseUtils;

import java.util.List;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;


/**
 * Created by Pawel on 2017-05-24.
 */

public class UserRepository {

    public final static String USERS_TAG = "USERS_TAG";
    public static final String USERS_NODE = "users";

    private FirebaseDatabase firebaseDatabase;

    public UserRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference usersNode(){
        return firebaseDatabase.getReference(USERS_NODE);
    }

    private DatabaseReference userNode(String uId){
        return usersNode().child(uId);
    }

    public Flowable<List<Key<User>>> onUserEvent(String uid) {
        return RxFirebaseDatabase.observeValueEvent(userNode(uid), RxDatabaseUtils.singletonListMapper(User::create));
    }

    public Maybe<Key<User>> user(String uid){
        return RxFirebaseDatabase.observeSingleValueEvent(userNode(uid),User::create);
    }

    public Maybe<String> userName(String uid){
        return RxFirebaseDatabase.observeSingleValueEvent(userNode(uid).child(User.USERNAME_FIELD),String.class);
    }

    public Maybe<String> userEmail(String uid){
        return RxFirebaseDatabase.observeSingleValueEvent(userNode(uid).child(User.EMAIL_FIELD),String.class);
    }

    public Single<Key<User>> updateUser(Key<User> user){
        return RxFirebaseDatabase.setValue(userNode(user.key()),user.value().toFirebaseValue())
                .toSingleDefault(Key.of(user.key(),user.value()));
    }

    public Completable updateUserName(String uid, String username){
        return RxFirebaseDatabase.setValue(userNode(uid).child(User.USERNAME_FIELD),username);
    }

    public Completable updateUserEmail(String uid, String email){
        return RxFirebaseDatabase.setValue(userNode(uid).child(User.EMAIL_FIELD),email);
    }
}
