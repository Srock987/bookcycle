package com.srock.bookcycle.data.repositories;

import android.net.Uri;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;

import durdinapps.rxfirebase2.RxFirebaseStorage;
import id.zelory.compressor.Compressor;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by pawelsrokowski on 14/09/2017.
 */

public class PhotoStorageRepository {

    public static final String PHOTOS_NODE = "photos";
    private final FirebaseStorage firebaseStorage;
    private Compressor compressor;
    private final String subdirectory;

    public PhotoStorageRepository(FirebaseStorage firebaseStorage, String subdirectory, Compressor compressor){
        this.firebaseStorage = firebaseStorage;
        this.subdirectory = subdirectory;
        this.compressor = compressor;
    }

    private StorageReference photosNode(){
        return firebaseStorage.getReference(PHOTOS_NODE).child(subdirectory);
    }

    private StorageReference aggregatedPhotosNode(String aggregationId){
        return photosNode().child(aggregationId);
    }

    public Single<UploadTask.TaskSnapshot> insertPhoto(String aggregationId, String photoId, File file){
        StorageReference reference = aggregatedPhotosNode(aggregationId).child(photoId);
        return compressor.compressToFileAsFlowable(file).singleOrError()
                .subscribeOn(Schedulers.computation())
                .map(Uri::fromFile)
                .observeOn(Schedulers.io())
                .flatMapMaybe(uri -> RxFirebaseStorage.putFile(reference,uri)).toSingle();
    }

    public Completable removePhoto(String aggregationId, String photoId){
        return RxFirebaseStorage.delete(photosNode().child(aggregationId).child(photoId));
    }


}
