package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.RequestedBookProposition;
import com.srock.bookcycle.utils.Firebase.Key;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by Pawel on 2017-09-29.
 */

public class RequestedPropositionRepository {

    private static final String REQUESTED_PROPOSITION_NODE = "requestedPropositions";
    private FirebaseDatabase firebaseDatabase;

    public RequestedPropositionRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference requestedPropositionsNode(){
        return firebaseDatabase.getReference(REQUESTED_PROPOSITION_NODE);
    }

    public Single<Key<RequestedBookProposition>> insertRequetedProposition(RequestedBookProposition requestedBookProposition, String key){
        return RxFirebaseDatabase.setValue(requestedPropositionsNode().child(key),requestedBookProposition.toFirebaseValue())
                .toSingleDefault(Key.of(key,requestedBookProposition));
    }

    public Completable removeRequestedProposition(String key){
        return RxFirebaseDatabase.setValue(requestedPropositionsNode().child(key),null);
    }
}
