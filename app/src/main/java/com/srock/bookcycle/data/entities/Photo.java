package com.srock.bookcycle.data.entities;

import android.support.annotation.Nullable;

import com.srock.bookcycle.utils.Firebase.Key;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;


import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-09-15.
 */

@AutoValue
@FirebaseValue
public abstract class Photo {

    public static final String URI_FILED_NAME = "uri";
    public static final String UPLOAD_STATUS_FIELD_NAME = "uploadStatus";

    public static Photo create(String uri, UploadStatus uploadStatus) {
        return new AutoValue_Photo(uri, uploadStatus);
    }

    public static Key<Photo> create(DataSnapshot dataSnapshot){
        return Key.of(dataSnapshot.getKey(), dataSnapshot.getValue(AutoValue_Photo.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_Photo.FirebaseValue(this);
    }

    @Nullable
    @PropertyName(URI_FILED_NAME)
    public abstract String uri();

    @FirebaseAdapter(UploadStatusTypeAdapter.class)
    @PropertyName(UPLOAD_STATUS_FIELD_NAME)
    public abstract UploadStatus uploadStatus();

    public enum UploadStatus{
        FINISHED,
        PENDING,
        NOT_STARTED,
        LOCAL
    }

    public static class UploadStatusTypeAdapter implements TypeAdapter<UploadStatus,String> {

        @Override
        public UploadStatus fromFirebaseValue(String value) {
            return UploadStatus.valueOf(value);
        }

        @Override
        public String toFirebaseValue(UploadStatus value) {
            return value.toString();
        }
    }
}

