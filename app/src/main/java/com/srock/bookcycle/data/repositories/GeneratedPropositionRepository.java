package com.srock.bookcycle.data.repositories;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.utils.Rx.RxDatabaseUtils;
import com.srock.bookcycle.data.entities.GeneratedBookProposition;
import com.srock.bookcycle.data.entities.Photo;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Flowable;
import io.reactivex.Single;

import static com.srock.bookcycle.data.utils.GeneratedPropositionHelper.createGeneratedBookProposition;

/**
 * Created by pawelsrokowski on 25/09/2017.
 */

public class GeneratedPropositionRepository {

    private final static String GENERATED_PROPOSITION_NODE = "generatedPropositions";

    private FirebaseDatabase firebaseDatabase;

    public GeneratedPropositionRepository(FirebaseDatabase firebaseDatabase){
        this.firebaseDatabase = firebaseDatabase;
    }

    private DatabaseReference generatedPropositionsNode(){
        return firebaseDatabase.getReference(GENERATED_PROPOSITION_NODE);
    }

    public Flowable<Key<GeneratedBookProposition>> listenForUpdate(String key){
        return RxFirebaseDatabase.observeValueEvent(generatedPropositionsNode().child(key),
                RxDatabaseUtils.singletonListMapper(GeneratedBookProposition::create))
                .filter(keys -> keys.size()!=0)
                .map(keys -> keys.get(keys.size()-1));
    }


    public Single<Key<GeneratedBookProposition>> updateGeneratedProposition(Key<Photo> photoKey){
        GeneratedBookProposition generatedBookProposition = createGeneratedBookProposition(photoKey.value().uri());
        return RxFirebaseDatabase.setValue(generatedPropositionsNode().child(photoKey.key()),generatedBookProposition.toFirebaseValue())
                .toSingleDefault(Key.of(photoKey.key(),generatedBookProposition));
    }


}
