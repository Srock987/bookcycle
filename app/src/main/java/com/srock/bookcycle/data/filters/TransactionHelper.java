package com.srock.bookcycle.data.filters;

import com.google.firebase.database.ServerValue;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by pawelsrokowski on 18/10/2017.
 */

public class TransactionHelper {
    public static Transaction createTransaction(Key<BookProposition> proportionKey,String curentUserId){
        BookProposition book = proportionKey.value();
        return Transaction.create(curentUserId,book.userProposingId(),null,proportionKey.key(),false,false, 0, Transaction.TransactionStatus.WAITING_FOR_RECIPIENT_PICK);
    }
}
