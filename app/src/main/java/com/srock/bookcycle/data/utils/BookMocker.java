package com.srock.bookcycle.data.utils;

import com.srock.bookcycle.R;
import com.srock.bookcycle.data.entities.BookProposition;

/**
 * Created by pawelsrokowski on 07/11/2017.
 */

public class BookMocker {
    public static final String BookError = "Something went wrong";

    public static BookProposition getBookMock(){
        return BookProposition.create(BookError,BookError,BookError,BookError,BookError,BookError,BookError);
    }

}
