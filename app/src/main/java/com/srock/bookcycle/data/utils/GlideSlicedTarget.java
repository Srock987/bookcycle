package com.srock.bookcycle.data.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

/**
 * Created by pawelsrokowski on 08/11/2017.
 */

public class GlideSlicedTarget extends SimpleTarget<Drawable> {

    private ImageView view;

    public GlideSlicedTarget(ImageView view){
        this.view = view;
    }


    @Override
    public void onResourceReady(Drawable resource, Transition<? super Drawable> transition) {
        view.setImageDrawable(resource);
    }
}
