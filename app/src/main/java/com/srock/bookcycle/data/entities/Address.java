package com.srock.bookcycle.data.entities;

import com.google.android.gms.location.places.Place;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.utils.Filters.AddressSurveyFilterUtils;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.Arrays;
import java.util.List;

import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-07-11.
 */

@AutoValue
@FirebaseValue
public abstract class Address {

    public static final String NAME_FIELD = "name";
    public static final String COUNTRY_FIELD = "country";
    public static final String REGION_FIELD = "region";
    public static final String POSTAL_CODE_FIELD = "postalCode";
    public static final String CITY_FIELD = "city";
    public static final String STREET_ADDRESS_FIELD = "streetAddress";
    public static final String USER_ID_FIELD = "userId";

    @PropertyName(NAME_FIELD)
    public abstract String name();
    @PropertyName(COUNTRY_FIELD)
    public abstract String country();
    @PropertyName(REGION_FIELD)
    public abstract String region();
    @PropertyName(POSTAL_CODE_FIELD)
    public abstract String postalCode();
    @PropertyName(CITY_FIELD)
    public abstract String city();
    @PropertyName(STREET_ADDRESS_FIELD)
    public abstract String streetAddress();
    @PropertyName(USER_ID_FIELD)
    public abstract String userId();

    public static Address create(String name, String country, String region, String postalCode, String city, String streetAddress,String uId) {
        return new AutoValue_Address(name, country, region, postalCode, city, streetAddress, uId);
    }

    public static Address create(Place place, String name,String uId){
        String address = String.valueOf(place.getAddress());
        List<String> addressList = Arrays.asList(address.split(","));
        return new AutoValue_Address(name,
                addressList.get(2),
                addressList.get(2),
                AddressSurveyFilterUtils.getFormatedPostalCode(addressList.get(1)),
                AddressSurveyFilterUtils.getFormatedCity(addressList.get(1)),
                addressList.get(0),
                uId);
    }

    public static Key<Address> create(DataSnapshot dataSnapshot){
        return Key.of(dataSnapshot.getKey(),dataSnapshot.getValue(AutoValue_Address.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_Address.FirebaseValue(this);
    }

}
