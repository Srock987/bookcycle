package com.srock.bookcycle.data.utils;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.GeneratedBookProposition;

/**
 * Created by Pawel on 2017-10-01.
 */

public class GeneratedPropositionHelper {

    public static GeneratedBookProposition createGeneratedBookProposition(String picLink){
        return GeneratedBookProposition.create("generatedBookName","generatedAuthor","gebnbookType","gendescr",picLink);
    }

    public static BookProposition fromGeneratedToBook(GeneratedBookProposition generatedBookProposition,
                                                      String userProposingId, String originCity){
        return BookProposition.create(generatedBookProposition.bookName(),
                generatedBookProposition.author(),
                generatedBookProposition.bookType(),
                generatedBookProposition.bookDescription(),
                generatedBookProposition.picLink(),
                userProposingId,
                originCity);
    }

}
