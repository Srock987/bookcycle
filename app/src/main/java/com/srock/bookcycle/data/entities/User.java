package com.srock.bookcycle.data.entities;

import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.utils.Firebase.Key;


import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

/**
 * Created by Pawel on 2017-05-24.
 */
@AutoValue
@FirebaseValue
public abstract class User {

    public static final String USERNAME_FIELD = "username";
    public static final String EMAIL_FIELD = "email";

    @PropertyName(USERNAME_FIELD)
    public abstract String username();
    @PropertyName(EMAIL_FIELD)
    public abstract String email();


    public static User create(String username, String email) {
        return new AutoValue_User(username, email);
    }

    public static Key<User> create(DataSnapshot dataSnapshot) {
        return Key.of(dataSnapshot.getKey(),dataSnapshot.getValue(AutoValue_User.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue() {
        return new AutoValue_User.FirebaseValue(this);
    }

}
