package com.srock.bookcycle.data.entities;

import android.os.Parcelable;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.PropertyName;
import com.srock.bookcycle.utils.Firebase.Key;
import me.mattlogan.auto.value.firebase.adapter.FirebaseAdapter;
import me.mattlogan.auto.value.firebase.adapter.TypeAdapter;
import me.mattlogan.auto.value.firebase.annotation.FirebaseValue;

@AutoValue
@FirebaseValue
public abstract class Transaction implements Parcelable {
    public static final String CREATOR_ID = "creatorId";
    public static final String RECIPIENT_ID = "recipientId";
    public static final String CREATOR_BOOK = "creatorBook";
    public static final String RECIPIENT_BOOK = "recipientBook";
    public static final String CREATOR_CONFIRMATION = "creatorConfirmation";
    public static final String RECIPIENT_CONFIRMATION = "recipientConfirmation";
    public static final String CREATION_TIMESTAMP = "creationTimestamp";
    public static final String TRANSACTION_STATUS = "transactionStatus";

    @PropertyName(CREATOR_ID)
    public abstract String creatorId();
    @PropertyName(RECIPIENT_ID)
    public abstract String recipientId();
    @Nullable
    @PropertyName(CREATOR_BOOK)
    public abstract String creatorBook();
    @PropertyName(RECIPIENT_BOOK)
    public abstract String recipientBook();
    @PropertyName(CREATOR_CONFIRMATION)
    public abstract boolean creatorConfirmation();
    @PropertyName(RECIPIENT_CONFIRMATION)
    public abstract boolean recipientConfirmation();
    @PropertyName(CREATION_TIMESTAMP)
    public abstract long creationTimestamp();
    @FirebaseAdapter(TransactionStatusTypeAdapter.class)
    @PropertyName(TRANSACTION_STATUS)
    public abstract TransactionStatus transactionStatus();


    public static Transaction create(String creatorId, String recipientId, String creatorBook, String recipientBook, boolean creatorConfirmation, boolean recipientConfirmation, long creationTimestamp, TransactionStatus transactionStatus) {
        return new AutoValue_Transaction(creatorId, recipientId, creatorBook, recipientBook, creatorConfirmation, recipientConfirmation, creationTimestamp, transactionStatus);
    }

    public static Key<Transaction> create(DataSnapshot dataSnapshot){
        return Key.of(dataSnapshot.getKey(),dataSnapshot.getValue(AutoValue_Transaction.FirebaseValue.class).toAutoValue());
    }

    public Object toFirebaseValue(){ return new AutoValue_Transaction.FirebaseValue(this);}

    public enum TransactionStatus{
        NOT_STARTED,
        WAITING_FOR_RECIPIENT_PICK,
        RECIPIENT_DENIAL,
        WAITING_FOR_CREATOR_CONFIRMATION,
        NO_SEND_STATUS,
        CREATOR_SEND,
        RECIPIENT_SEND,
        BOTH_SEND,
        CREATOR_RECEIVED,
        RECIPIENT_RECEIVED,
        BOTH_RECEIVED,
        TRANSACTION_ENDED
    }

    public static class TransactionStatusTypeAdapter implements TypeAdapter<TransactionStatus,String>{

        @Override
        public TransactionStatus fromFirebaseValue(String value) {return TransactionStatus.valueOf(value);}

        @Override
        public String toFirebaseValue(TransactionStatus value) {
            return value.toString();
        }
    }
}
