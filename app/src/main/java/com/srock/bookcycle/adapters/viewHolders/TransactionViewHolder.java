package com.srock.bookcycle.adapters.viewHolders;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.srock.bookcycle.R;
import com.srock.bookcycle.activities.TransactionDetailActivity;
import com.srock.bookcycle.adapters.FlowableAdapter;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.utils.TransactionUtils;
import com.srock.bookcycle.presenters.TransactionItemPresenter;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.custom.TransactionView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pawelsrokowski on 26/10/2017.
 */

public class TransactionViewHolder extends FlowableAdapter.ReactiveViewHolder<Key<Transaction>> implements TransactionView {

    private TransactionItemPresenter presenter = new TransactionItemPresenter(this);

    @BindView(R.id.topBookTitleTextView)
    TextView topTitle;
    @BindView(R.id.bottomBookTitleTextView)
    TextView bottomTitle;
    @BindView(R.id.topBookCoverImageView)
    ImageView leftImageView;
    @BindView(R.id.bottomBookCoverImageView)
    ImageView rightImageView;

    private Context context;


    public TransactionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        this.context = itemView.getContext();
        itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, TransactionDetailActivity.class);
            intent.putExtra(TransactionUtils.transactionKey,presenter.getTransactionKey());
            context.startActivity(intent);
        });
    }

    @Override
    public void setItem(Key<Transaction> transactionKey) {
        presenter.setTransactionKey(transactionKey);
        presenter.showTransaction();
    }

    @Override
    public Key<Transaction> getItem() {
        return null;
    }

    @Override
    public void setTopTitle(String title) {
        topTitle.setText(title);
    }

    @Override
    public void setLeftImageView(String pictureUrl) {
        Glide.with(context).load(pictureUrl).into(leftImageView).onLoadFailed(context.getDrawable(R.drawable.book_mock));
    }

    @Override
    public void setBottomTitle(String title) {
        bottomTitle.setText(title);
    }

    @Override
    public void setRightImageView(String pictureUrl) {
        Glide.with(context).load(pictureUrl).into(rightImageView);//.onLoadFailed(context.getDrawable(R.drawable.book_mock));
    }


}
