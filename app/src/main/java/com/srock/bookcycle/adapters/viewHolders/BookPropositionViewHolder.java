package com.srock.bookcycle.adapters.viewHolders;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.srock.bookcycle.R;
import com.srock.bookcycle.adapters.FlowableAdapter;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.fragments.fragmentsPresenters.BookPresenter;
import com.srock.bookcycle.fragments.fragmentsViews.BookView;
import com.srock.bookcycle.utils.Firebase.Key;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pawelsrokowski on 10/10/2017.
 */

public class BookPropositionViewHolder extends FlowableAdapter.ReactiveViewHolder<Key<BookProposition>> implements BookView {

    @BindView(R.id.bookTitleTextView)
    TextView bookTitleTextView;
    @BindView(R.id.bookAuthorTextView)
    TextView bookAuthorTextView;
    @BindView(R.id.descriptionTextView)
    TextView descriptionTextView;
    @BindView(R.id.bookCoverImageView)
    ImageView bookCoverImageView;

    private BookPresenter presenter = new BookPresenter(this);
    private Context context;

    public BookPropositionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        context=itemView.getContext();
    }

    @Override
    public void setItem(Key<BookProposition> bookPropositionKey) {
        presenter.setBookPropositionKey(bookPropositionKey);
        Log.d("BookViewHolder","BookKey set");
        presenter.showBookProposition();
    }

    public Key<BookProposition> getItem() {
        return presenter.getBookPropositionKey();
    }

    @Override
    public void setTitle(String title) {
        bookTitleTextView.setText(title);
    }

    @Override
    public void setAuthor(String author) {
        bookAuthorTextView.setText(author);
    }

    @Override
    public void setDescription(String description) {
        descriptionTextView.setText(description);
    }

    @Override
    public void setImageView(String pictureURL) {
        Glide.with(context).load(pictureURL).into(bookCoverImageView).onLoadFailed(context.getDrawable(R.drawable.book_mock));
    }

}
