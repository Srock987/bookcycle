package com.srock.bookcycle.adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;


import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by pawelsrokowski on 10/10/2017.
 */

public class FlowableAdapter<T> extends RecyclerView.Adapter<FlowableAdapter.ReactiveViewHolder<T>>
{
    private final Flowable<List<T>> listFlowable;
    private List<T> currentList;
    private final ReactiveViewHolderFactory<T> viewHolderFactory;

    public FlowableAdapter(Flowable<List<T>> listFlowable, ReactiveViewHolderFactory<T> viewHolderFactory) {
        this.viewHolderFactory = viewHolderFactory;
        this.currentList = Collections.emptyList();
        this.listFlowable = listFlowable;
        this.listFlowable.observeOn(AndroidSchedulers.mainThread()).subscribe(items ->{
            this.currentList = items;
            this.notifyDataSetChanged();;
        });
    }

    @Override
    public ReactiveViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        ReactiveViewHolderFactory.ViewAndHolder<T>  viewAndHolder = viewHolderFactory.createViewAndHolder(parent,viewType);
        return viewAndHolder.viewHolder;
    }

    @Override
    public void onBindViewHolder(ReactiveViewHolder<T> holder, int position) {
        T item = currentList.get(position);
        holder.setItem(item);
    }

    public T getItemAtPosition(int position){
        return currentList.get(position);
    }

    @Override
    public int getItemCount() {
        return currentList.size();
    }

    public static abstract class ReactiveViewHolder<T> extends RecyclerView.ViewHolder{

        public ReactiveViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void setItem(T t);
        public abstract T getItem();
    }

    public interface ReactiveViewHolderFactory<T> {

        class ViewAndHolder<T> {
            public final View view;
            public final ReactiveViewHolder<T> viewHolder;


            public ViewAndHolder(View view,ReactiveViewHolder<T> viewHolder) {
                this.view = view;
                this.viewHolder = viewHolder;
            }
        }

        ViewAndHolder<T> createViewAndHolder(@NonNull ViewGroup parent, int viewType);
    }
}
