package com.srock.bookcycle.views.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.srock.bookcycle.R;
import com.srock.bookcycle.data.entities.BookProposition;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by pawelsrokowski on 08/11/2017.
 */

public class BookView extends ConstraintLayout {

    @BindView(R.id.bookTitleTextView)
    TextView bookTitleTextView;
    @BindView(R.id.bookAuthorTextView)
    TextView bookAuthorTextView;
    @BindView(R.id.bookCoverImageView)
    ImageView bookCoverImageView;
    @BindView(R.id.descriptionTextView)
    TextView bookDescriptionTextView;

    public BookView(Context context) {
        super(context);
        inflate(getContext(),R.layout.book_item,this);
        ButterKnife.bind(this);
    }

    public BookView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.BookView,0,0);
        init(a);
    }

    public BookView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context.getTheme().obtainStyledAttributes(attrs, R.styleable.BookView,0,0));
    }


    private void init(TypedArray array){
        inflate(getContext(),R.layout.book_item,this);
        ButterKnife.bind(this);
        try {
            setTitle(array.getString(R.styleable.BookView_bookTitle));
            setAuthor(array.getString(R.styleable.BookView_bookAuthor));
            setDescription(array.getString(R.styleable.BookView_bookDescription));
            setCover(array.getString(R.styleable.BookView_bookCoverUrl));
        } finally {
            array.recycle();
        }
    }

    public void setBook(BookProposition bookProposition){
        updateTitle(bookProposition.bookName());
        updateAuthor(bookProposition.author());
        updateDescription(bookProposition.bookDescription());
        updateCover(bookProposition.picLink());
        invalidate();
        requestLayout();
    }

    public void setTitle(String title){
        updateTitle(title);
        invalidate();
        requestLayout();
    }

    private void updateTitle(String title){
        bookTitleTextView.setText(title);
    }

    public void setAuthor(String author){
        updateAuthor(author);
        invalidate();
        requestLayout();
    }

    private void updateAuthor(String author){
        bookAuthorTextView.setText(author);
    }

    public void setDescription(String description){
        updateDescription(description);
        invalidate();
        requestLayout();
    }

    private void updateDescription(String description) {
        bookDescriptionTextView.setText(description);
    }

    public void setCover(String coverUrl){
        updateCover(coverUrl);
        invalidate();
        requestLayout();
    }

    private void updateCover(String coverUrl) {
        Glide.with(getContext()).load(coverUrl).into(bookCoverImageView);
    }

    public void setPickBook(){
        updateTitle(getContext().getString(R.string.pick_book));
        Glide.with(getContext()).load(R.drawable.book_mock).into(bookCoverImageView);
        invalidate();
        requestLayout();
    }


}
