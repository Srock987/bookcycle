package com.srock.bookcycle.views.custom;

/**
 * Created by pawelsrokowski on 26/10/2017.
 */

public interface TransactionView {
    void setTopTitle(String title);
    void setLeftImageView(String pictureUrl);
    void setBottomTitle(String title);
    void setRightImageView(String pictureUrl);
}
