package com.srock.bookcycle.views.activity;

import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.List;

import io.reactivex.Flowable;

public interface TransactionsListView {
    void showTransactions(Flowable<List<Key<Transaction>>> transactionListFlowable);
}
