package com.srock.bookcycle.views.activity;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Pawel on 2017-07-14.
 */

public interface BookSearchActivityView {
    void showBooks(Flowable<List<Key<BookProposition>>> bookListFlowable);
}
