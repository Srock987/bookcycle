package com.srock.bookcycle.views.activity;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by pawelsrokowski on 09/11/2017.
 */

public interface TransactionDetailView {
    void showCreatorsBook(BookProposition creatorsBook);
    void showRecipientBook(BookProposition recipientBook);
    void showPickCreators();
    void showPickRecipients();
}
