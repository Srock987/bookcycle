package com.srock.bookcycle.presenters.interfaces;

/**
 * Created by pawelsrokowski on 09/11/2017.
 */

public interface TransactionDetailPresenterInterface {
    void fetchBooks();
    void onPause();
}
