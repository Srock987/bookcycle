package com.srock.bookcycle.presenters.interfaces;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by pawelsrokowski on 26/10/2017.
 */

public interface TransactionsListPresenterInterface {
    void onResume();
    void onDestroy();
    void onSave();
    void transactionClick(Key<Transaction> transactionKey);
}
