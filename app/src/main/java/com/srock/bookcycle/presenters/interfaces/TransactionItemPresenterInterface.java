package com.srock.bookcycle.presenters.interfaces;

/**
 * Created by pawelsrokowski on 26/10/2017.
 */

public interface TransactionItemPresenterInterface {
    void showTransaction();
}
