package com.srock.bookcycle.presenters.interfaces;

import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.utils.Firebase.Key;

/**
 * Created by Pawel on 2017-07-14.
 */

public interface BookSearchPresenterInterface {
    void onResume();
    void onDestroy();
    void onSave();
    void onWant(Key<BookProposition> bookPropositionKey);
}
