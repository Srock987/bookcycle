package com.srock.bookcycle.presenters;

import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.repositories.TransactionRepository;
import com.srock.bookcycle.domain.transaction.UserTransactionUseCase;
import com.srock.bookcycle.presenters.interfaces.TransactionsListPresenterInterface;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.activity.TransactionsListView;

/**
 * Created by pawelsrokowski on 25/10/2017.
 */

public class TransactionsListPresenter implements TransactionsListPresenterInterface {

    private TransactionsListView transactionsListView;

    public TransactionsListPresenter(TransactionsListView transactionsListView) {
        this.transactionsListView = transactionsListView;
    }


    @Override
    public void onResume() {
        UserTransactionUseCase useCase = new UserTransactionUseCase(new TransactionRepository(FirebaseDatabase.getInstance()));
        transactionsListView.showTransactions(useCase.getCurrentUserCreatedTransactions());
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onSave() {

    }

    @Override
    public void transactionClick(Key<Transaction> transactionKey) {

    }
}
