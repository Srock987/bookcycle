package com.srock.bookcycle.presenters;

import android.util.Log;

import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.data.repositories.TransactionRepository;
import com.srock.bookcycle.domain.bookProposition.GetUserPropositionsUseCase;
import com.srock.bookcycle.domain.transaction.TransactionInsertUseCase;
import com.srock.bookcycle.presenters.interfaces.BookSearchPresenterInterface;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.activity.BookSearchActivityView;

/**
 * Created by Pawel on 2017-07-14.
 */

public class BookSearchPresenter implements BookSearchPresenterInterface {

    private BookSearchActivityView bookSearchActivityView;

    public BookSearchPresenter(BookSearchActivityView bookSearchActivityView) {
        this.bookSearchActivityView = bookSearchActivityView;
    }

    @Override
    public void onResume() {
        fetchBookProposition();
    }

    private void fetchBookProposition() {
        GetUserPropositionsUseCase useCase = new GetUserPropositionsUseCase(new BookRepository(FirebaseDatabase.getInstance()));
        bookSearchActivityView.showBooks(useCase.getCurrentUserBooks());
    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onSave() {

    }

    @Override
    public void onWant(Key<BookProposition> bookPropositionKey) {
        TransactionInsertUseCase insertUseCase = new TransactionInsertUseCase(new TransactionRepository(FirebaseDatabase.getInstance()));
        insertUseCase.insertTransactionFromBookProposition(bookPropositionKey).subscribe(transactionKey -> {
            Log.d("TRANSACTION","INSERTED");
        });
    }

}
