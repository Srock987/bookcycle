package com.srock.bookcycle.presenters;

import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.domain.bookProposition.GetBookPropositionUseCase;
import com.srock.bookcycle.presenters.interfaces.TransactionDetailPresenterInterface;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.activity.TransactionDetailView;

/**
 * Created by pawelsrokowski on 09/11/2017.
 */

public class TransactionDetailPresenter implements TransactionDetailPresenterInterface {

    TransactionDetailView transactionDetailView;
    Key<Transaction> transactionKey;

    public TransactionDetailPresenter(TransactionDetailView transactionDetailView) {
        this.transactionDetailView = transactionDetailView;
    }

    public Key<Transaction> getTransactionKey() {
        return transactionKey;
    }

    public void setTransactionKey(Key<Transaction> transactionKey) {
        this.transactionKey = transactionKey;
    }

    @Override
    public void fetchBooks() {
        GetBookPropositionUseCase useCase = new GetBookPropositionUseCase(new BookRepository(FirebaseDatabase.getInstance()));
        if (transactionKey.value().creatorBook()!=null) {
            useCase.getBookProposition(transactionKey.value().creatorBook()).subscribe(bookPropositionKey -> {
                transactionDetailView.showCreatorsBook(bookPropositionKey.value());
            });
        }else {
            transactionDetailView.showPickCreators();
        }
        if (transactionKey.value().recipientBook()!=null) {
            useCase.getBookProposition(transactionKey.value().recipientBook()).subscribe(bookPropositionKey -> {
                transactionDetailView.showRecipientBook(bookPropositionKey.value());
            });
        }else {
            transactionDetailView.showPickRecipients();
        }
    }


    @Override
    public void onPause() {

    }
}
