package com.srock.bookcycle.presenters;

import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.repositories.BookRepository;
import com.srock.bookcycle.domain.bookProposition.GetBookPropositionUseCase;
import com.srock.bookcycle.presenters.interfaces.TransactionItemPresenterInterface;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.custom.TransactionView;

/**
 * Created by pawelsrokowski on 26/10/2017.
 */

public class TransactionItemPresenter implements TransactionItemPresenterInterface {

    private TransactionView transactionView;
    private Key<Transaction> transactionKey;

    public TransactionItemPresenter(TransactionView transactionView) {
        this.transactionView = transactionView;
    }

    public Key<Transaction> getTransactionKey() {
        return transactionKey;
    }

    public void setTransactionKey(Key<Transaction> transactionKey) {
        this.transactionKey = transactionKey;
    }

    @Override
    public void showTransaction() {
        GetBookPropositionUseCase useCase = new GetBookPropositionUseCase(new BookRepository(FirebaseDatabase.getInstance()));
        if (transactionKey.value().creatorBook()!=null) {
            useCase.getBookProposition(transactionKey.value().creatorBook()).subscribe(bookPropositionKey -> {
                transactionView.setTopTitle(bookPropositionKey.value().bookName());
                transactionView.setLeftImageView(bookPropositionKey.value().picLink());
            });
        }
        if (transactionKey.value().recipientBook()!=null) {
            useCase.getBookProposition(transactionKey.value().recipientBook()).subscribe(bookPropositionKey -> {
                transactionView.setBottomTitle(bookPropositionKey.value().bookName());
                transactionView.setRightImageView(bookPropositionKey.value().picLink());
            });
        }
    }

}
