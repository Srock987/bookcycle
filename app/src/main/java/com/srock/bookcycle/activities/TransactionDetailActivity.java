package com.srock.bookcycle.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.srock.bookcycle.R;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.data.utils.TransactionUtils;
import com.srock.bookcycle.presenters.TransactionDetailPresenter;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.activity.TransactionDetailView;
import com.srock.bookcycle.views.custom.BookView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TransactionDetailActivity extends AppCompatActivity implements TransactionDetailView {

    @BindView(R.id.creatorBook)
    BookView creatorBookView;
    @BindView(R.id.recipientBook)
    BookView recipientBookView;

    private TransactionDetailPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        ButterKnife.bind(this);
        presenter = new TransactionDetailPresenter(this);
        injectTransactionKey();
    }

    private void injectTransactionKey(){
        Key<Transaction> transactionKey = getIntent().getParcelableExtra(TransactionUtils.transactionKey);
        if (transactionKey!=null){
            presenter.setTransactionKey(transactionKey);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.fetchBooks();
    }


    @Override
    public void showCreatorsBook(BookProposition creatorsBook) {
        creatorBookView.setBook(creatorsBook);
    }

    @Override
    public void showRecipientBook(BookProposition recipientBook) {
        recipientBookView.setBook(recipientBook);
    }

    @Override
    public void showPickCreators() {
        creatorBookView.setPickBook();
    }

    @Override
    public void showPickRecipients() {
        recipientBookView.setPickBook();
    }
}
