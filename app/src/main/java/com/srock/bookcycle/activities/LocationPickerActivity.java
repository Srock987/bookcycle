package com.srock.bookcycle.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.srock.bookcycle.data.repositories.AddressRepository;
import com.srock.bookcycle.fragments.AddressFragmentConfirmationListener;
import com.srock.bookcycle.fragments.AddressSurveyFragment;
import com.srock.bookcycle.data.entities.Address;
import com.srock.bookcycle.fragments.LoadingFragment;
import com.srock.bookcycle.R;

/**
 * Created by Pawel on 2017-05-29.
 */

public class LocationPickerActivity extends AppCompatActivity implements AddressFragmentConfirmationListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_picker);
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
        getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();
        autocompleteFragment.setFilter(typeFilter);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                showAddressSurveyFragment(place);
            }
            @Override
            public void onError(Status status) {
            }
        });
    }

    private void showAddressSurveyFragment(Place place){
        Address address = Address.create(place,"pawel",FirebaseAuth.getInstance().getCurrentUser().getUid());
        AddressSurveyFragment addressFragment = new AddressSurveyFragment();
        addressFragment.setAddress(address);
        addressFragment.setAddressFragmentConfirmationListener(this);
        getFragmentManager().beginTransaction().replace(R.id.survay_fragment,addressFragment).commit();
    }

    @Override
    public void onConfirmationClicked(Address address) {
        AddressRepository addressRepository = new AddressRepository(FirebaseDatabase.getInstance());
        LoadingFragment loadingFragment = new LoadingFragment();
        getFragmentManager().beginTransaction().replace(R.id.survay_fragment,loadingFragment).commit();

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        addressRepository.insertAddress(firebaseAuth.getCurrentUser().getUid(),address).subscribe(addressKey -> {
            AddressSurveyFragment addressSurveyFragment = new AddressSurveyFragment();
            addressSurveyFragment.setAddress(addressKey.value());
            getFragmentManager().beginTransaction().replace(R.id.survay_fragment,addressSurveyFragment).commit();
        }) ;
    }
}
