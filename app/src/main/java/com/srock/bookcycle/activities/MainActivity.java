package com.srock.bookcycle.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.srock.bookcycle.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Pawel on 2017-05-11.
 */

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;

    @BindView(R.id.my_toolbar)
    Toolbar toolbar;
    @BindView(R.id.center_button)
    Button centerButton;
    @BindView(R.id.addBookPropostionButton)
    Button addBookPropositionButton;
    @BindView(R.id.seeBookPropositionButton)
    Button seeBookPropositionButton;
    @BindView(R.id.startUpladingPhotoButton)
    Button startUploadingPhotoButton;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        setSupportActionBar(toolbar);
        centerButton.setOnClickListener(v -> {
                startLocationActivity();
        });
        addBookPropositionButton.setOnClickListener(v -> {
           startTransactionsActivity();
        });
        seeBookPropositionButton.setOnClickListener(v -> {
            startBookPropositionActivity();
        });
        startUploadingPhotoButton.setOnClickListener(view -> {
            startCreateProportionActivity();
        });
    }

    private void startCreateProportionActivity() {
        Intent createPropositionIntent = new Intent(MainActivity.this, CreatePropositionActivity.class);
        startActivity(createPropositionIntent);
    }

    private void startBookPropositionActivity() {
        Intent bookPropositionIntent = new Intent(MainActivity.this,BookSearchActivity.class);
        startActivity(bookPropositionIntent);
    }

    private void startTransactionsActivity(){
        Intent transactionsIntent = new Intent(MainActivity.this,TransactionsListActivity.class);
        startActivity(transactionsIntent);
    }


    private void startLocationActivity(){
        startActivity(new Intent(MainActivity.this,LocationPickerActivity.class));
    }

}
