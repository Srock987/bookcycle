package com.srock.bookcycle.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.srock.bookcycle.adapters.FlowableAdapter;
import com.srock.bookcycle.adapters.viewHolders.BookPropositionViewHolder;
import com.srock.bookcycle.data.entities.BookProposition;
import com.srock.bookcycle.presenters.BookSearchPresenter;
import com.srock.bookcycle.presenters.interfaces.BookSearchPresenterInterface;
import com.srock.bookcycle.views.activity.BookSearchActivityView;
import com.srock.bookcycle.R;
import com.srock.bookcycle.utils.Firebase.Key;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;


public class BookSearchActivity extends AppCompatActivity implements BookSearchActivityView, View.OnClickListener {

    private BookSearchPresenterInterface presenter;

    @BindView(R.id.bookSwipeRecyclerView)
    RecyclerView bookSwipeRecyclerView;
    @BindView(R.id.wantButton)
    Button wantButton;

    private FlowableAdapter<Key<BookProposition>> flowableAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_search_proposition);
        ButterKnife.bind(this);
        presenter = new BookSearchPresenter(this);
        SnapHelper snapHelper = new PagerSnapHelper();
        layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false);
        snapHelper.attachToRecyclerView(bookSwipeRecyclerView);
        bookSwipeRecyclerView.setLayoutManager(layoutManager);
        wantButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


    @Override
    public void showBooks(Flowable<List<Key<BookProposition>>> bookListFlowable) {
        FlowableAdapter.ReactiveViewHolderFactory<Key<BookProposition>> viewHolderFactory = (parent, viewType) -> {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_book_proposition,parent,false);
            return new FlowableAdapter.ReactiveViewHolderFactory.ViewAndHolder<>(view, new BookPropositionViewHolder(view));
        };
        flowableAdapter = new FlowableAdapter<>(bookListFlowable, viewHolderFactory);
        bookSwipeRecyclerView.setAdapter(flowableAdapter);
    }

    @Override
    public void onClick(View view) {
        presenter.onWant(flowableAdapter.getItemAtPosition(layoutManager.findFirstCompletelyVisibleItemPosition()));
    }
}
