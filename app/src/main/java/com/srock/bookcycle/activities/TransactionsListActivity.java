package com.srock.bookcycle.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.srock.bookcycle.R;
import com.srock.bookcycle.adapters.FlowableAdapter;
import com.srock.bookcycle.adapters.viewHolders.TransactionViewHolder;
import com.srock.bookcycle.data.entities.Transaction;
import com.srock.bookcycle.presenters.TransactionsListPresenter;
import com.srock.bookcycle.utils.Firebase.Key;
import com.srock.bookcycle.views.activity.TransactionsListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Flowable;

/**
 * Created by pawelsrokowski on 25/10/2017.
 */

public class TransactionsListActivity extends AppCompatActivity implements TransactionsListView {

    private TransactionsListPresenter presenter;

    @BindView(R.id.transactionRecyclerView)
    RecyclerView transactionRecycleView;
    @BindView(R.id.transactionProgressBar)
    ProgressBar progressBar;

    private FlowableAdapter<Key<Transaction>> flowableAdapter;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_list);
        ButterKnife.bind(this);
        layoutManager = new GridLayoutManager(this,2);
        presenter = new TransactionsListPresenter(this);
        transactionRecycleView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideRecyclerView();
        presenter.onResume();
    }

    @Override
    public void showTransactions(Flowable<List<Key<Transaction>>> transactionListFlowable) {
        FlowableAdapter.ReactiveViewHolderFactory<Key<Transaction>> viewHolderFactory = (parent, viewType) -> {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_item,parent,false);
            return new FlowableAdapter.ReactiveViewHolderFactory.ViewAndHolder<>(view, new TransactionViewHolder(view));
        };
        transactionListFlowable.subscribe(keys -> {
           if (!keys.isEmpty()){
               showRecyclerView();
           }
        });
        flowableAdapter = new FlowableAdapter<>(transactionListFlowable, viewHolderFactory);
        transactionRecycleView.setAdapter(flowableAdapter);
    }

    private void showRecyclerView(){
        progressBar.setVisibility(View.GONE);
        transactionRecycleView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideRecyclerView();
    }

    private void hideRecyclerView(){
        progressBar.setVisibility(View.VISIBLE);
        transactionRecycleView.setVisibility(View.GONE);
    }
}
