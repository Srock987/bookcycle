package com.srock.bookcycle.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.srock.bookcycle.fragments.TakePhotoFragment;
import com.srock.bookcycle.R;

/**
 * Created by pawelsrokowski on 14/09/2017.
 */

public class CreatePropositionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivity_create_proposition);
        showPhotoFragment();
    }

    private void showPhotoFragment(){
        getFragmentManager().beginTransaction().replace(R.id.propositionFragment,new TakePhotoFragment()).commit();
    }
}
