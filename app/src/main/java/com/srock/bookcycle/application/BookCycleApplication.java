package com.srock.bookcycle.application;

import android.app.Application;

import com.google.firebase.FirebaseApp;

/**
 * Created by pawelsrokowski on 13/09/2017.
 */

public class BookCycleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
    }
}
