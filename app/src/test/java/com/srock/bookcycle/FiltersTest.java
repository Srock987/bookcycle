package com.srock.bookcycle;

import com.srock.bookcycle.utils.Filters.AddressSurveyFilterUtils;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Pawel on 2017-07-11.
 */

public class FiltersTest {
    private final String inputAdress = "30-725 Kraków";
    private final String expectedPostalResult = "30-725";
    private final String expectedCityResult = "Kraków";

    @Test
    public void postalCodeFilter() throws Exception{
        String filteredPostal = AddressSurveyFilterUtils.getFormatedPostalCode(inputAdress);
        assertTrue(expectedPostalResult.equals(filteredPostal));
    }

    @Test
    public void cityFilter() throws Exception{
        String filteredCity = AddressSurveyFilterUtils.getFormatedCity(inputAdress);
        assertTrue(expectedCityResult.equals(filteredCity));
    }
}
