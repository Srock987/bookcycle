var functions = require('firebase-functions');
var admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

exports.createUserAccount = functions.auth.user().onCreate(event => {
    // [END onCreateTrigger]
    // [START eventAttributes]
    const user = event.data; // The Firebase user.

const uid = user.uid;
const email = user.email; // The email of the user.
const displayName = user.displayName; // The display name of the user.
// [END eventAttributes]fire

return addUserToDatabase(uid, displayName, email);
});

function addUserToDatabase(uid, displayName, email) {
    console.log('New user account created', uid, displayName, email);
    return admin.database().ref("users").child(uid).set({username: displayName, email: email});
}


exports.generateBookProposition = functions.database.ref("requestedPropositions/{pushId}").onCreate(event => {
    const picLink = event.data.child("picLink").val();
    const pushId = event.params.pushId;
    scrapeWebSite('http://www.empik.com/wilcze-leze-pilipiuk-andrzej,p1142982852,ksiazka-p')
return addGeneratedPropositionToDatabase(pushId,"genName","genBookType","genBookDescription","genAuthor",picLink)
});

function scrapeWebSite(url) {
    request(url,function (error, response, html) {
        if (!error){
            console.log("Worked");
            let $ = cheerio.load(html);
            let json = {title:"",author:"",bookType:"",description:""};

            $('main.page-product').filter(function () {
                let data = $(this);
                console.log('main');
                title = data.children().first().children().last().children().last().children().first().children().first().children().first().children().last().text();
                bookType = data.children().first().children().first().children().first().children().first().children().first().children().first().children().last().text();
                json.title = title;
                json.bookType = bookType;
                console.log(json)
            })
        }else {
            console.log("Error")
        }
    })
}


function addGeneratedPropositionToDatabase(id, genBookName, genBookType, genBookDescription, genAuthor, pictureURL) {
    console.log('New generated proposition created', genBookName, genBookType, genBookDescription, genAuthor, pictureURL);
    return admin.database().ref("generatedPropositions").child(id)
        .set({
            bookName: genBookName,
            bookType: genBookType,
            bookDescription: genBookDescription,
            author: genAuthor,
            picLink: pictureURL
        })
}